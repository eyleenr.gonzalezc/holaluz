package com.holaluz;

import com.holaluz.application.controller.ReadingsController;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class HolaluzApplication implements CommandLineRunner {
    private final ReadingsController clientDataController;

    public static void main(String[] args) {
        SpringApplication.run(HolaluzApplication.class, args);
    }

    @Override
    public void run(String... args) {
        clientDataController.printClientSuspiciousReadings(args[0]);
    }
}
