package com.holaluz.infrastructure.repository;

import com.holaluz.domain.model.Client;
import com.holaluz.domain.model.Reading;
import com.holaluz.domain.repository.ReadingRepository;
import com.opencsv.CSVReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;

import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository(value = "CSVReadingRepository")
public class CSVReadingRepository implements ReadingRepository {

    @Override
    public List<Client> getReadingsByClient(String resourceName) {

        Map<String, LinkedHashSet<Reading>> csvResult = new HashMap<>();

        try (CSVReader reader = new CSVReader(new InputStreamReader(new ClassPathResource(resourceName).getInputStream()))) {

            List<String[]> lines = reader.readAll();
            lines.remove(0);
            lines.forEach(line -> {
                csvResult.computeIfAbsent(line[0], k -> new LinkedHashSet<>());
                csvResult.get(line[0]).add(new Reading(LocalDate.of(Integer.parseInt((line[1].substring(0, 3))),
                        Integer.parseInt(line[1].substring(5)), 1), Double.parseDouble(line[2])));
            });


        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        return csvResult.entrySet().stream().map(e -> Client.builder().clientId(e.getKey()).readings(e.getValue()).build()).collect(Collectors.toList());
    }

}
