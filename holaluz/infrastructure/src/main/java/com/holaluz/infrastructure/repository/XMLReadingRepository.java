package com.holaluz.infrastructure.repository;

import com.holaluz.domain.model.Client;
import com.holaluz.domain.model.Reading;
import com.holaluz.domain.repository.ReadingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Repository(value = "XMLReadingRepository")
public class XMLReadingRepository implements ReadingRepository {

    private final Logger logger = LoggerFactory.getLogger(ReadingRepository.class);

    @Override
    public List<Client> getReadingsByClient(String resourceName) {

        Map<String, LinkedHashSet<Reading>> xmlResult = new HashMap<>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try (InputStream is = readXmlFileIntoInputStream(resourceName)) {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(is);
            if (doc.hasChildNodes()) {
                NodeList nodeList = doc.getChildNodes().item(0).getChildNodes();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node tempNode = nodeList.item(i);
                    if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                        NamedNodeMap nodeMap = tempNode.getAttributes();
                        String clientId = nodeMap.item(0).getNodeValue();
                        String period = nodeMap.item(1).getNodeValue();
                        xmlResult.computeIfAbsent(clientId, k -> new LinkedHashSet<>());
                        xmlResult.get(clientId).add(new Reading(LocalDate.of(Integer.parseInt((period.substring(0, 3))),
                                Integer.parseInt(period.substring(5)), 1),
                                Double.parseDouble(tempNode.getTextContent())));
                    }

                }
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }


        return xmlResult.entrySet().stream().map(e -> Client.builder().clientId(e.getKey()).readings(e.getValue()).build()).collect(Collectors.toList());
    }

    private static InputStream readXmlFileIntoInputStream(final String resourceName) throws IOException {
        return new ClassPathResource(resourceName).getInputStream();
    }
}
