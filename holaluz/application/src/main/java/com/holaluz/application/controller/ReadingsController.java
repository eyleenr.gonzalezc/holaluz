package com.holaluz.application.controller;

import com.holaluz.domain.usecase.GetClientReadingsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class ReadingsController {

    private final GetClientReadingsUseCase getClientReadingsUseCase;


    public void printClientSuspiciousReadings(final String resourceName) {
        getClientReadingsUseCase.getClientReadings(resourceName);
    }
}
