package com.holaluz.application.usecase;

import com.holaluz.domain.model.Client;
import com.holaluz.domain.model.Reading;
import com.holaluz.domain.repository.ReadingRepository;
import com.holaluz.domain.usecase.GetClientReadingsUseCase;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetClientReadingsUseCaseImpl implements GetClientReadingsUseCase {

    private final Logger logger = LoggerFactory.getLogger(GetClientReadingsUseCase.class);

    @Autowired
    @Qualifier("CSVReadingRepository")
    private ReadingRepository csvRepository;

    @Autowired
    @Qualifier("XMLReadingRepository")
    private ReadingRepository xmlRepository;

    @Override
    public void getClientReadings(String resourceName) {

        logger.debug("getClientReadings method reached");
        String extension = FilenameUtils.getExtension(resourceName);
        List<Client> clients = new ArrayList<>();

        if (StringUtils.equals(extension, "csv")) {
            clients.addAll(csvRepository.getReadingsByClient(resourceName));
        } else if (StringUtils.equals(extension, "xml")) {
            clients.addAll(xmlRepository.getReadingsByClient(resourceName));
        }

        logger.info("| Client\t| Month\t| Suspicious\t| Median\n");
        logger.info(" -------------------------------------------------------------------------------\n");
        clients.forEach(client -> {
            List<Double> clientReadings = client.getReadings().stream().map(Reading::getValue).toList();
            double median = getMedian(clientReadings);

            printSuspiciousReadings(client, median);

        });

    }

    private static double getMedian(List<Double> clientReadings) {
        clientReadings = clientReadings.stream().sorted().toList();
        int halfValue = clientReadings.size() / 2;
        if (clientReadings.size() % 2 == 0) {
            return (clientReadings.get(halfValue - 1) + clientReadings.get(halfValue)) / 2;
        }
        return clientReadings.get(halfValue);
    }

    private void printSuspiciousReadings(Client client, double median) {

        client.getReadings().forEach(reading -> {
            if (reading.getValue() < median/2 || reading.getValue() > median+(median/2)) {
                logger.info("| {}\t| {}\t| {}\t| {}\n", client.getClientId(), reading.getReadingPeriod().getMonth(), reading.getValue(), median);
            }
        });
    }
}
