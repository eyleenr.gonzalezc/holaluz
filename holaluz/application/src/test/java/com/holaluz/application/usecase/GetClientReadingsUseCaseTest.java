package com.holaluz.application.usecase;

import com.holaluz.domain.model.Client;
import com.holaluz.domain.model.Reading;
import com.holaluz.domain.repository.ReadingRepository;
import com.holaluz.domain.usecase.GetClientReadingsUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GetClientReadingsUseCaseTest {

    @InjectMocks
    private GetClientReadingsUseCaseImpl getClientReadingsUseCase;

    @Mock
    private ReadingRepository readingRepository;

    @Test
    @DisplayName("getReadingsByClientId with valid args")
    void getReadingsByClientId() {
        // Arrange
        final String resourceName = "2016-readings.csv";
        LinkedHashSet<Reading> readings = new LinkedHashSet<>();
        readings.add(new Reading(LocalDate.of(2016, 03, 1), 41039));
        List<Client> clients = new ArrayList<>();
        clients.add(new Client("583ef6329e372", readings));
        when(readingRepository.getReadingsByClient(anyString())).thenReturn(clients);

        // Act
        getClientReadingsUseCase.getClientReadings(resourceName);

        // Assert
        verify(readingRepository).getReadingsByClient(anyString());

    }
}
