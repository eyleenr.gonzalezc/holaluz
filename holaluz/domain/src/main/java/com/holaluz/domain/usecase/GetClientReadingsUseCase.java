package com.holaluz.domain.usecase;

import com.holaluz.domain.model.Reading;

import java.util.Set;

public interface GetClientReadingsUseCase {

    void getClientReadings(final String resourceName);
}
