package com.holaluz.domain.repository;

import com.holaluz.domain.model.Client;

import java.util.List;

public interface ReadingRepository {

    List<Client> getReadingsByClient(final String resourceName);

}
